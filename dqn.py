import gym
import keras
# import tensorflow.keras as keras
import numpy as np
import random
from collections import deque
# keras = tf.keras
import chaser_ai

class DQNAgent:
    def __init__(self, state_size, action_size):
        self.state_size = state_size
        self.action_size = action_size
        self.mem = deque(maxlen=20000)
        self.gamma = 0.95    # discount rate
        self.epsilon = 1  # exploration rate
        self.epsilon_min = 0.001
        self.epsilon_decay = 0.0001
        self.learning_rate = 0.001
        self.model = self.build_model()

    def build_model(self, model=None):
        if not model:
            model = keras.models.Sequential([
                keras.layers.Dense(128, input_shape=self.state_size),
                keras.layers.Dense(128),
                keras.layers.Dense(256),
                keras.layers.Dense(self.action_size, activation='linear')
            ])
        model.compile(loss='mse',
                      optimizer=keras.optimizers.Adam(lr=self.learning_rate))
        model.summary()
        return model

    def remember(self, state, action, reward, next_state, done):
        target = reward
        if not done:
            target = reward + self.gamma * \
                     np.amax(self.model.predict(next_state)[0])
        target_f = self.model.predict(state)
        target_f[0][action] = target
        self.mem.append((state, target_f))

    def act(self, state, other):
        if np.random.rand() <= self.epsilon:
            return random.randrange(self.action_size)
            # return 0 if chaser_ai.pong_ai(*other) == 'down' else 1
        act_values = self.model.predict(state)
        return np.argmax(act_values[0])  # returns action

    def replay(self, state, action, reward, next_state, done):
        # x = []
        # y = []
        # for state, target_f in self.mem:
        #     self.model.fit(state, target_f)
        target = reward
        if not done:
            target = reward + self.gamma * \
                     np.amax(self.model.predict(next_state)[0])
        target_f = self.model.predict(state)
        target_f[0][action] = target
        self.model.fit(state, target_f, verbose=0)
        self.epsilon = self.epsilon-self.epsilon_decay if self.epsilon > self.epsilon_min else self.epsilon_min


dqn = DQNAgent((14*2,), 2)
dqn.build_model(model=keras.models.load_model('model3.h5'))
dqn.epsilon = 0.001
last_state = None
frames = 0
last_op = 0
last_self = 0
last_r = False
last_positions = None
last_action = None


def pong_ai(paddle_frect, other_paddle_frect, ball_frect, table_size, scores):
    op_score = scores[0]
    self_score = scores[1]
    other = (paddle_frect, other_paddle_frect, ball_frect, table_size, scores)
    global last_state, frames, last_op, last_positions, last_r, last_self, last_action
    state = np.array((paddle_frect.pos[0],paddle_frect.pos[1], ball_frect.pos[0], ball_frect.pos[1], paddle_frect.size[0], paddle_frect.size[1], ball_frect.size[0], ball_frect.size[1], table_size[0], table_size[1], other_paddle_frect.pos[0], other_paddle_frect.pos[1], other_paddle_frect.size[0], other_paddle_frect.size[1]))
    # np.expand_dims(state, axis=0)
    state_b = state
    # print(state.shape)

    if type(last_positions) == np.ndarray:
        state = np.hstack([last_positions, state])
        # print(state)
        state = np.expand_dims(state, axis=0)

        action = dqn.act(state, other)
    else:
        action = 0
    if abs(paddle_frect.pos[0] - ball_frect.pos[0] + ball_frect.size[0]) < 3 and not last_r:
        last_r = True
        done = False
        reward = table_size[1] - abs(ball_frect.pos[1] - (paddle_frect.pos[1] + paddle_frect.size[1])/2)
    elif self_score == last_self + 1:
        reward = 0
        done = True
        last_r = False
    elif op_score == last_op + 1:
        done = True
        reward = 0
        last_r = False
    # elif abs(ball_frect.pos[0] - paddle_frect.pos[0]) < ball_frect.size[0] and (paddle_frect.pos[1] - ball_frect.pos[1]) >= ball_frect.size[1] and (paddle_frect.pos[1] - ball_frect.pos[1]) >= -paddle_frect.size[1]:
    elif paddle_frect.intersect(ball_frect) or ball_frect.intersect(paddle_frect) and not last_r:
        done = False
        last_r=True
        # print('r')
        reward = table_size[1] - abs(ball_frect.pos[1] - (paddle_frect.pos[1] + paddle_frect.size[1])/2)
    else:
        done = False
        last_r = False
        reward = 0
    if type(last_positions) == np.ndarray:
        if reward != 0:
            print(reward)
        dqn.replay(last_state, last_action, reward, state, done)
        # dqn.replay() if done else 0
        done = False
    last_positions = state_b
    if frames == 0:
        state = np.hstack([last_positions, state])
        state = np.expand_dims(state, axis=0)
    frames += 1
    last_state = state
    last_op = op_score
    last_self = self_score
    last_action = action
    # print(op_score)
    if frames % 5000 == 0:
        print(frames)
        dqn.model.save('model4.h5')
    if action == 0:
        return 'down'
    else:
        return 'up'


